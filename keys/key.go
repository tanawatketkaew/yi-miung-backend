package keys

import (
	"crypto/rsa"
	"github.com/dgrijalva/jwt-go"
	"io/ioutil"
	"path/filepath"
)

func Getkey() (*rsa.PrivateKey,*rsa.PublicKey){
	keyPrivatePath,_ := filepath.Abs("./keys/key")
	key,err := ioutil.ReadFile(keyPrivatePath)
	if err != nil{
		panic(err.Error())
	}
	privateKey,err := jwt.ParseRSAPrivateKeyFromPEM(key)
	if err != nil{
		panic(err.Error())
	}
	keyPublicPath,_ := filepath.Abs("./keys/key.pub")
	key,err = ioutil.ReadFile(keyPublicPath)
	if err != nil{
		panic(err.Error())
	}
	publicKey,err := jwt.ParseRSAPublicKeyFromPEM(key)
	if err != nil{
		panic(err.Error())
	}
	return privateKey,publicKey
}