package controllers

import (
	"bytes"
	"github.com/labstack/echo/v4"
	"gopkg.in/gomail.v2"
	"html/template"
	"os"
	"time"
	"yi-miung-backend/database"
	"yi-miung-backend/models"
)

func SendMailReceivePassword(c echo.Context)error {
	email := c.FormValue("email")

	user := new(models.Users)

	errQueryData := database.SQL.Raw(`select name,id_user from users where email = ?`,email).Scan(user)
	if errQueryData.Error != nil{
		panic(errQueryData.Error.Error())
	}


	newPassword := RandomString()
	newPasswordHash,_ := HashPassword(newPassword)


	dataInEmail := new(models.EmailResetPassword)

	dataInEmail.NameCustomer = user.Name
	dataInEmail.NewPassword = newPassword
	dataInEmail.DateTimeResetPassword = time.Now().Format("02/01/06") + " " + time.Now().Format("03:04:05 PM")

	Template := template.New("reset_password.html")
	Template, err := Template.ParseFiles("./templates/Resetpassword/reset_password.html")
	if err != nil {
		return err
	}

	var tpl bytes.Buffer
	if err := Template.Execute(&tpl, dataInEmail); err != nil {
		return err
	}



	result := tpl.String()
	m := gomail.NewMessage()

	m.SetHeader("From", os.Getenv("Email"), os.Getenv("NameCompany"))
	m.SetHeader("To", email)
	m.SetHeader("Subject", "การ Reset Password ของ Application Yiming Mandarin")
	m.SetBody("text/html", result)

	emailBeforeSend := gomail.NewDialer("smtp.gmail.com", 587, os.Getenv("Email"), os.Getenv("Password"))

	if errSendmail := emailBeforeSend.DialAndSend(m); errSendmail  != nil {
		panic(errSendmail .Error())
	}

	errUpdatePassword := database.SQL.Debug().Exec("Update users set password = ? where id_user = ?",newPasswordHash,user.IdUser).Error
	if errUpdatePassword != nil{
		panic(errUpdatePassword.Error())
	}

	return c.JSON(200,"ok")
}