package controllers

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"yi-miung-backend/database"
	"yi-miung-backend/models"
)

func GetPackage(c echo.Context)error{
	data := new([]models.Package)
	errQueryData := database.SQL.Raw(`select * from package`).Scan(data).Error
	if errQueryData != nil{
		panic(errQueryData.Error())
	}
	for index,_ := range *data{
		(*data)[index].PathImagePackage = (*data)[index].PathImagePackage[1:]
	}
	return c.JSON(http.StatusOK,echo.Map{
		"data":data,
		"errMessage":nil,
		"status":200,
	})
}