package controllers

import (
	"bytes"
	"encoding/base64"
	"fmt"
	_ "fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
	"yi-miung-backend/database"
	"yi-miung-backend/keys"
	"yi-miung-backend/models"
)

var validate *validator.Validate





func Login(c echo.Context)error{
	Request := new(models.Login)
	if err := c.Bind(Request); err != nil {
		panic(err.Error())
	}

	validate = validator.New()

	data := new(models.UsersResponse)

	errValidate := validate.Struct(Request)
	if errValidate != nil{
		return c.JSON(http.StatusBadRequest,echo.Map{
			"errMessage":errValidate.Error(),
		})
	}


	if Request.TypeLogin == os.Getenv("LOGIN_TYPE"){
		errQueryDataUser := database.SQL.Debug().Raw(`select 
	users.password,
	users.email,
    users.name,
	user_package.package_id_package,
	users.id_user,
	path_image_profile
	from users 
	left 
	join user_package on users.id_user = user_package.user_id where users.email = ?`,Request.Email).Scan(data)


		if errQueryDataUser.RowsAffected == 0 {
			return c.JSON(http.StatusBadRequest,echo.Map{
				"errMessage":"email or password invalid",
				"status":400,
			})
		}

		if errQueryDataUser.Error != nil{
			return c.JSON(http.StatusInternalServerError,echo.Map{
				"errMessage":"Database down contact to admin",
				"status":500,
			})
		}
		if ComparePasswordHashAndPasswordInput(Request.Password,data.Password){
			fmt.Println(ComparePasswordHashAndPasswordInput(Request.Password,data.Password))
			Token := GenerateToken(data.Email,data.PackageIdPackage,3,60,strconv.Itoa(data.IdUser),os.Getenv("LOGIN_TYPE"))
			return c.JSON(http.StatusOK,echo.Map{
				"status":200,
				"errMessage":nil,
				"token":Token["access"],
				"email":data.Email,
				"name":data.Name,
				"path_image":data.PathImageProfile[1:],
				"type_login":os.Getenv("LOGIN_TYPE"),
			})
		}else{
			return c.JSON(http.StatusBadRequest,echo.Map{
				"errMessage":"email or password invalid",
				"status":400,
				"token":nil,
			})
		}


	}else if Request.TypeLogin == os.Getenv("FACEBOOK_LOGIN"){
		errQueryCheckData := database.SQL.Debug().Raw(`select id_for_facebook_account from users where id_for_facebook_account = ?`,Request.IdForFacebook).Scan(data)
		if errQueryCheckData.RowsAffected == 0{
			errUpdateDataFacebook := database.SQL.Exec(`Insert into users(id_user,name,path_image_profile,id_for_facebook_account,created_at) values(?,?,?,?,?)`,nil,Request.NameOfUser,Request.PictureImage,Request.IdForFacebook,time.Now()).Error
			if errUpdateDataFacebook != nil{
				panic(errUpdateDataFacebook.Error())
			}
			Token := GenerateToken("",0,3,60,Request.IdForFacebook,os.Getenv("FACEBOOK_LOGIN"))
			return c.JSON(http.StatusOK,echo.Map{
				"status":200,
				"errMessage":nil,
				"token":Token["access"],
				"email":data.Email,
				"name":Request.NameOfUser,
				"path_image":Request.PictureImage,
				"type_login":os.Getenv("FACEBOOK_LOGIN"),
			})
		}else{
			errUpdateData := database.SQL.Exec("Update users set path_image_profile = ? where id_for_facebook_account = ?",Request.PictureImage,Request.IdForFacebook).Error
			if errUpdateData != nil{
				panic(errUpdateData.Error())
			}
			Token := GenerateToken("",0,3,60,Request.IdForFacebook,os.Getenv("FACEBOOK_LOGIN"))
			return c.JSON(http.StatusOK,echo.Map{
				"status":200,
				"errMessage":nil,
				"token":Token["access"],
				"email":Request.Email,
				"name":Request.NameOfUser,
				"path_image":Request.PictureImage,
				"type_login":os.Getenv("FACEBOOK"),
			})
		}
		if errQueryCheckData.Error != nil && errQueryCheckData.RowsAffected == 0{
			panic(errQueryCheckData.Error.Error())
		}

	}else{
		return c.JSON(http.StatusBadRequest,echo.Map{
			"errMessage":"login type invalid",
			"status":400,
			"token":nil,
		})
	}
	return nil
}


func SingUpUser(c echo.Context)error{
	user := new(models.UsersResponse)
	Name := c.FormValue("name")
	Email := c.FormValue("email")
	Password := c.FormValue("password")
	Images := c.FormValue("images")



	errSelectEmail := database.SQL.Debug().Raw(`select email from users where email = ?`,Email).Scan(user)
	if errSelectEmail.RowsAffected > 0{
		return c.JSON(http.StatusBadRequest,echo.Map{
			"status":400,
			"errMessage":"Email Duplicate key",
		})
	}
	if errSelectEmail.RowsAffected != 0 && errSelectEmail.Error != nil{
		panic(errSelectEmail.Error.Error())
	}

	PasswordHash,_ := HashPassword(Password)

	pathname := ""
	random := strconv.Itoa(rand.Int())
	idx := strings.Index(Images, ";base64,")
	if idx < 0 {
		panic("InvalidImage")
	}
	ImageType := Images[11:idx]
	log.Println(ImageType)

	unbased, err := base64.StdEncoding.DecodeString(Images[idx+8:])
	if err != nil {
		panic("Cannot decode b64")
	}
	r := bytes.NewReader(unbased)
	switch ImageType {
	case "png":
		im, err := png.Decode(r)
		if err != nil {
			panic("Bad png")
		}
		pathname =  fmt.Sprintf("./static/images/%s.%s", random,"png" )
		f, err := os.OpenFile(pathname, os.O_WRONLY|os.O_CREATE, 0777)
		if err != nil {
			panic("Cannot open file")
		}

		png.Encode(f, im)
	case "jpeg":
		im, err := jpeg.Decode(r)
		if err != nil {
			panic("Bad jpeg")
		}
		pathname =  fmt.Sprintf("./static/images/%s.%s", random,"jpeg" )
		f, err := os.OpenFile(pathname, os.O_WRONLY|os.O_CREATE, 0777)
		if err != nil {
			panic("Cannot open file")
		}

		jpeg.Encode(f, im, nil)
	case "gif":
		im, err := gif.Decode(r)
		if err != nil {
			panic("Bad gif")
		}
		pathname =  fmt.Sprintf("./static/images/%s.%s", random,"gif" )
		f, err := os.OpenFile(pathname, os.O_WRONLY|os.O_CREATE, 0777)
		if err != nil {
			panic("Cannot open file")
		}

		gif.Encode(f, im, nil)
	}

	errSignUpUsers := database.SQL.Exec(`Insert into users(id_user,name,email,password,path_image_profile,created_at) values(?,?,?,?,?,?)`,nil,Name,Email,PasswordHash,pathname[1:],time.Now())
	if errSignUpUsers.Error != nil{
		panic(errSignUpUsers.Error.Error())

	}

	return c.JSON(http.StatusOK,echo.Map{
		"status":200,
		"errMessage":nil,
	})
}


///////

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func ComparePasswordHashAndPasswordInput(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func GenerateToken(email string, permissions int, DurationExp int, DurationAccess int, id string,TypeLogin string) map[string]string {
	now := time.Now()
	if DurationExp == 0 {
		exp := now.Add(time.Duration(time.Minute * time.Duration(DurationAccess)))
		claims := jwt.NewWithClaims(jwt.SigningMethodRS256, models.Token{
			Email:      email,
			UserPackage: permissions,
			StandardClaims: jwt.StandardClaims{
				IssuedAt:  now.Unix(),
				ExpiresAt: exp.Unix(),
			},
			TokenType: "access",
			Id:        id,
			TypeLogin:TypeLogin,
		})
		PrivateKey, _ := keys.Getkey()
		token, err := claims.SignedString(PrivateKey)
		if err != nil {
			panic(err)
		}
		return map[string]string{
			"access":  token,
			"refresh": "",
		}
	} else {
		exp := now.Add(time.Duration(time.Minute * time.Duration(DurationAccess)))
		refreshexp := now.Add(time.Duration(time.Hour * time.Duration(DurationExp)))
		claims := jwt.NewWithClaims(jwt.SigningMethodRS256, models.Token{
			Email:      email,
			UserPackage: permissions,
			StandardClaims: jwt.StandardClaims{
				IssuedAt:  now.Unix(),
				ExpiresAt: exp.Unix(),
			},
			TokenType: "access",
			Id:        id,
		})
		refresh := jwt.NewWithClaims(jwt.SigningMethodRS256, models.Token{
			Email:      email,
			UserPackage: permissions,
			StandardClaims: jwt.StandardClaims{
				IssuedAt:  now.Unix(),
				ExpiresAt: refreshexp.Unix(),
			},
			TokenType: "refresh",
			Id:        id,
		})
		PrivateKey, _ := keys.Getkey()
		AccessToken, err := claims.SignedString(PrivateKey)
		if err != nil {
			panic(err)
		}
		RefreshToken, err2 := refresh.SignedString(PrivateKey)
		if err2 != nil {
			panic(err2.Error())
		}
		return map[string]string{
			"access":  AccessToken,
			"refresh": RefreshToken,
		}
	}
}


func CreateFile(files *[]models.Files) error {
	for _, file := range *files {
		src, err := file.Files.Open()
		if err != nil {
			return err
		}
		defer src.Close()
		des, err := os.Create(file.Path)
		if err != nil {
			return err
		}
		defer des.Close()
		if _, err := io.Copy(des, src); err != nil {
			return err
		}
	}
	return nil
}

func RandomString() string {
	const charset = "abcdefghijklmnopqrstuvwxyz" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

	b := make([]byte, 8)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)

}
