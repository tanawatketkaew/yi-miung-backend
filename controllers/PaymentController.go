package controllers

import (
	"github.com/labstack/echo"
	"log"
	"github.com/omise/omise-go"
	"github.com/omise/omise-go/operations"
	"net/http"
	"os"
	"yi-miung-backend/models"
)

func PaymentCourse(c echo.Context)error{
	RequestPayment := new(models.CardPayment)
	if errBindData := c.Bind(RequestPayment);errBindData != nil{
		panic(errBindData.Error())
	}
	client, e := omise.NewClient(os.Getenv("OmisePublicKey"), os.Getenv("OmiseSecretKey"))
	if e != nil {
		log.Fatal(e)
	}

	token, createToken := &omise.Token{}, &operations.CreateToken{
		Name:            RequestPayment.NameAccount,
		Number:          RequestPayment.CardNumber,
		ExpirationMonth: RequestPayment.ExpirationMonth,
		ExpirationYear:  RequestPayment.ExpirationYear,
	}
	if e := client.Do(token, createToken); e != nil {
		return c.JSON(http.StatusInternalServerError,echo.Map{
			"status":500,
			"errMessage":e,
		})
	}

	charge, createCharge := &omise.Charge{}, &operations.CreateCharge{
		Amount:   RequestPayment.PriceOfPackage, // ฿ 1,000.00
		Currency: "thb",
		Card:     token.ID,
	}
	if e := client.Do(charge, createCharge); e != nil {
		return c.JSON(http.StatusInternalServerError,echo.Map{
			"status":500,
			"errMessage":e,
		})
	}

	return c.JSON(http.StatusOK,echo.Map{
		"status":200,
		"errMessage":nil,
	})
	//log.Printf("charge: %s  amount: %s %d\n", charge.ID, charge.Currency, charge.Amount)
}
