package main

import (
	"github.com/labstack/echo/v4"
	"os"
	"yi-miung-backend/configs"
	"yi-miung-backend/database"
	"yi-miung-backend/routes"
)

func main(){
	configs.Init("dev")
	e := echo.New()
	routes.Route(e)
	database.ConnectDatabase()
	defer database.SQL.Close()
	e.Logger.Fatal(e.Start(":" + os.Getenv("PORT")))
}
