package middlewares

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"net/http"
	"yi-miung-backend/keys"
	"yi-miung-backend/models"
)

func SetCORS(e *echo.Echo) {
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete},
	}))
}
func LogURL(e *echo.Echo) {
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "time=${time_rfc3339_nano}, method=${method}, url=${host}${uri}, status=${status}, remote_ip=${remote_ip}, error:${error}\n",
	}))
}

func MiddlewareLoginBefore() echo.MiddlewareFunc{
	_,publicKey := keys.Getkey()
	return middleware.JWTWithConfig(middleware.JWTConfig{
		TokenLookup: "header:" + echo.HeaderAuthorization,
		SigningMethod: "RS256",
		AuthScheme:   "Bearer",
		Claims:    &models.Token{},
		SigningKey:  publicKey,
	})
}
