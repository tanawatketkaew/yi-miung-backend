package models

import "time"

type CardPayment struct {
	NameAccount string `json:"name_account"`
	CardNumber  string `json:"card_number"`
	ExpirationMonth time.Month `json:"expiration_month"`
	ExpirationYear int `json:"expiration_year"`
	PriceOfPackage int64 `json:"price_of_package"`
}
