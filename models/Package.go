package models

import "time"

type Package struct {
	IdPackage int `json:"id_package"`
	NamePackage string `json:"name_package"`
	PathImagePackage string `json:"path_image_package"`
	CreatedAt   time.Time `json:"created_at"`
	CreatedBy   string 	  `json:"created_by"`
	ColorCodeBackground string `json:"color_code_background"`
	PricePackage  int `json:"price_package"`
	Period        int `json:"period"`
}
