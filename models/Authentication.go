package models


type Login struct {
	Email string  `json:"email" validate:"required,email"`
	Password string  `json:"password" validate:"required"`
	TypeLogin string `json:"type_login" validate:"required"`
	IdForFacebook string `json:"id_for_facebook"`
	PictureImage  string `json:"picture_image"`
	NameOfUser 	  string `json:"name_of_user"`
}

type EmailResetPassword struct {
	NameCustomer string `json:"name_customer"`
	TokenResetPassword string `json:"token_reset_password"`
	DateTimeResetPassword string `json:"time_reset"`
	NewPassword 		 string `json:"new_password"`
}

