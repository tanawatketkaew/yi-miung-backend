package models

import "time"

type UsersResponse struct {
	IdUser int `json:"id_user"`
	Name   string `json:"name"`
	Email  string  `json:"email"`
	Password string `json:"password"`
	PathImageProfile string `json:"path_image_profile"`
	IdForFacebookAccount string `json:"id_for_facebook_account"`
	CardNumber string `json:"card_number"`
	ExpireCardNumber string `json:"expire_card_number"`
	RefreshToken   string `json:"refresh_token"`
	PackageIdPackage int `json:"package_id_package"`
}

type Users struct {
	IdUser int `json:"id_user"`
	Name   string `json:"name"`
	Email  string  `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
	PathImageProfile string `json:"path_image_profile"`
	IdForFacebookAccount string `json:"id_for_facebook_account"`
	CardNumber string `json:"card_number"`
	ExpireCardNumber string `json:"expire_card_number" `
	RefreshToken   string `json:"refresh_token"`
	CreatedAt time.Time `json:"created_at"`
}
