package models


import "mime/multipart"

type Files struct {
	Files  *multipart.FileHeader
	Path   string
	Header string
}
type Images struct {
	Name   string
	Header string
}