package models

import "github.com/dgrijalva/jwt-go"

type Token struct {
	Email      string
	UserPackage int
	jwt.StandardClaims
	TokenType string
	Id        string
	TypeLogin string
}
