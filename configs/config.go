package configs

import (
	"github.com/joho/godotenv"
	"path/filepath"
)

func Init(env string) {
	switch env {
	case "dev":
		path, _ := filepath.Abs("./configs/dev.env")
		err := godotenv.Load(path)
		if err != nil {
			panic(err)
		}
	case "prod":
		path, _ := filepath.Abs("./configs/prod.env")
		err := godotenv.Load(path)
		if err != nil {
			panic(err)
		}
	}

}
