package routes

import (
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/echo/v4"
	"yi-miung-backend/controllers"
	"yi-miung-backend/middlewares"
)

func Route(e *echo.Echo){
	middlewares.SetCORS(e)
	middlewares.LogURL(e)
	e.Use(middleware.Static("./"))
	route := e.Group("/api/v1")

	Authentication := route.Group("/auth")
	Authentication.POST("/login",controllers.Login)
	Authentication.POST("/sign_up",controllers.SingUpUser)
	Authentication.GET("/reset_password",controllers.SendMailReceivePassword)


	Payment := route.Group("/payment")
	Payment.GET("/get_package",controllers.GetPackage)
}