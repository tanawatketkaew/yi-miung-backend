package database

import (
	_ "github.com/go-sql-driver/mysql"
	"fmt"
	"github.com/jinzhu/gorm"
	"log"
	"os"
)

var SQL *gorm.DB

func ConnectDatabase(){
	connectionString := fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		os.Getenv("DB_USERNAME"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"))
	db, err := gorm.Open("mysql", connectionString)
	if err != nil {
		log.Println(err)
		SQL = db
	}else{
		log.Println("connected database")
		SQL = db
	}
}
